Utilitaire de conversion: fichier de backup SMS VMG vers XML pour app Android SMS Backup Restore

Création d’un petit script permettant de transformer nos gentils petits fichiers vmg récupérer par une sauvegarde de nos messages SMS. C’est bien gentils de récup un fichier de backup, encore faut il pouvoir le réimporter et avec le passage des dumbphones vers les smartphone, les formats ont bien évolués…. Et pas compatible évidement.

VMG est un format créé par Nokia pour la sauvegarde de ses sms, utilisé par d’autres constructeurs (Samsung entre autre) il est très commun dans le monde des dumbphones. Voici donc un petit script pour vous permettre de transformer ces sauvegardes en fichiers XML compréhensible par SMS Backup Restore une app Android assez populaire. :)

Je ne donnerai pas les détails de comment récupérer vos fichiers de sauvegarde, ni même comment effectuer cette sauvegarde chaque téléphone étant différents et c’est pas les modèles différents qui manquent. Je présume donc que vous avez vos fichiers xml dans un dossier backup. Il va falloir au préalable concaténer les fichiers vmg afin de les traiter tous d’un coup et de ne pouvoir créer qu’un unique fichier xml. En faisant cela, il sera bien plus aisé et rapide de restaurer les sms avec l’app, d’un seul coup. Le script est simple et suppose que vous ayez vos sms écris en français. Il ne gère pas les langues étrangères mais il est très simple de pouvoir modifier ce comportement en changeant la ligne 15 dans une autre langue:

locale.setlocale(locale.LC_ALL, 'fr_FR')

Pour concaténer vos fichiers vmg et les convertir :

$ cat ./backup/*vmg > ./backup/allsms_backuped.vmg
$ vmg2xml.py -i ./backup/allsms_backuped.vmg -o ./backup/allsms_android.xml

Ensuite prenez le fichier xml générer et importer par une restauration avec l’app SMS Backup Restore.

Vous pouvez télécharger le script ici.

Problèmes connus : Tous les sms restaurés auront leur date au “17 Janvier 1970”. Un correctif va être apporté.
Correction apportée, SMS Backup Restore utilise un timestamp incluant les milliseconds… Lien de téléchargement mis à jour vous pouvez utiliser le même qu’auparavant dans ce post.
